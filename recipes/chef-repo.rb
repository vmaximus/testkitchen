# This recipe creates the directory structure under /etc/chef-repo
# and populates the users .bash_profile with the correct path and
# ruby version

# Generate /etc/chef-repo directory structure
script 'create chef-repo' do
  interpreter 'bash'
  cwd '/etc'
  code <<-EOH
  /opt/chefdk/bin/chef generate repo chef-repo
  EOH
end

# Create the data directory for shared folders between virtualbox instances and the host
directory '/etc/chef-repo/data' do
  mode 0755
  action :create
end

# users can source this file or copy its contents to their .bash_profile
template '/etc/chef-repo/.chefrc' do
  source 'chefrc.erb'
  owner 'root'
  group 'wheel'
  mode '0755'
end

# attempts to automate the above for centos and mac users.
# THIS WILL WIPE THE .bash_profile FILE FOR MACs

case node['platform']
when 'centos'
  # This ruby loop with only apply to the following users
  %w(vagrant apena vphan).each do |users|
    script 'setenv' do
      interpreter 'bash'
      user users
      code <<-EOH
      cat /etc/chef-repo/.chefrc >> /home/#{users}/.bash_profile
      EOH
      only_if { ::File.directory?("/home/#{users}") }
      only_if { ::File.readlines("/home/#{users}/.bash_profile").grep(/chefdk/).size == 0 }
    end
  end

when 'mac_os_x'
  script 'set chef-repo permissions' do
    interpreter 'bash'
    user 'root'
    code <<-EOH
    chown -R root:admin /etc/chef-repo
    chmod -R 0775 /etc/chef-repo
    EOH
  end
  # This ruby loop with only apply to the following users
  %w(vagrant apena vphan).each do |users|
    template "/Users/#{users}/.bash_profile" do
      source 'mac_bash_profile.erb'
      owner users
      mode '0755'
      only_if { ::File.directory?("/Users/#{users}") }
    end
  end
end

template '/etc/chef-repo/cookbooks/sample-Berksfile' do
  source 'Berksfile.erb'
  owner 'root'
  mode '0755'
end

template '/etc/chef-repo/cookbooks/.sample-kitchen.yml' do
  source 'kitchen.yml.erb'
  owner 'root'
  mode '0755'
end
