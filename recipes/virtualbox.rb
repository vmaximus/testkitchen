# The dmg file for mac 10.10.5 and 10.11.1 (Yosemite & El Cap) are identical

case node['platform']
when 'mac_os_x'

  sw_id = data_bag_item('software', 'virtualbox')['id']
  sw_version = data_bag_item('software', 'virtualbox')['version']
  download_url = data_bag_item('software', 'virtualbox')['mac1010-download_src']
  dmg_name = data_bag_item('software', 'virtualbox')['dmg']

  dmg_package 'VirtualBox' do
    volumes_dir 'VirtualBox'
    dmg_name "#{dmg_name}"
    source "#{download_url}"
    type 'pkg'
    action :install
    not_if do ::File.exist?('/usr/bin/VBoxManage') end
  end

when 'centos'
  sw_id = data_bag_item('software', 'virtualbox')['id']
  sw_version = data_bag_item('software', 'virtualbox')['version']
  download_url = data_bag_item('software', 'virtualbox')['download_src']
  rpm_file = data_bag_item('software', 'virtualbox')['centos6_rpm']

  remote_file "/root/#{rpm_file}" do
    source "#{download_url}"
    action :create_if_missing
  end

  script 'install virtualbox' do
    interpreter 'bash'
    cwd '/root'
    code <<-EOH
    yum -y localinstall /root/#{rpm_file}
    EOH
    not_if do ::File.exist?('/usr/bin/VBoxManage') end
  end
end
