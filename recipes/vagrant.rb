# The dmg file for os x version 10.10.5 and 10.11.1 (Yosemite & El Cap) are identical

case node['platform']
when 'mac_os_x'

  sw_id = data_bag_item('software', 'vagrant')['id']
  download_url = data_bag_item('software', 'vagrant')['mac1010-download_src']

  dmg_package 'Vagrant' do
    source "#{download_url}"
    type 'pkg'
    action :install
    not_if do ::File.exist?('/opt/vagrant/bin/vagrant') end
  end
  #################################################
  %w(vagrant adycus dnegrotto rbiltz sroy vphan).each do |users|
    vagrant_plugin 'vagrant-omnibus' do
      version '1.4.1'
      user users
      only_if { ::File.directory?("/Users/#{users}") }
    end
  end
  #################################################

when 'centos'

  sw_id = data_bag_item('software', 'vagrant')['id']
  sw_version = data_bag_item('software', 'vagrant')['version']
  download_url = data_bag_item('software', 'vagrant')['download_src']
  rpm_file = data_bag_item('software', 'vagrant')['centos6_rpm']

  remote_file "/root/#{rpm_file}" do
    source "#{download_url}"
    action :create_if_missing
  end

  script 'install vagrant' do
    interpreter 'bash'
    cwd '/root'
    code <<-EOH
    yum -y localinstall /root/#{rpm_file}
    EOH
    not_if do ::File.exist?('/opt/vagrant/bin/vagrant') end
  end
  ####################################
  %w(vagrant adycus dnegrotto rbiltz sroy vphan).each do |users|
    vagrant_plugin 'vagrant-omnibus' do
      version '1.4.1'
      user users
      only_if { ::File.directory?("/home/#{users}") }
    end
  end
  ####################################
end
