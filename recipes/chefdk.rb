# There are specific dmg files for mac 10.10.5 and 10.11.1 (Yosemite & El Cap).
# Therefore we nest case statements for platform_version under platform
case node['platform']
when 'mac_os_x'
  case node['platform_version']
  when '10.10.5'
    sw_id = data_bag_item('software', 'chefdk')['id']
    sw_version = data_bag_item('software', 'chefdk')['version']
    download_url = data_bag_item('software', 'chefdk')['mac1010-download_src']
    dmg_name = data_bag_item('software', 'chefdk')['dmg']

    dmg_package 'chefdk-0.10.0-1' do
      volumes_dir 'Chef Development Kit'
      dmg_name "#{sw_id}-#{sw_version}"
      source "#{download_url}"
      type 'pkg'
      action :install
      not_if do ::File.exist?('/opt/chefdk/bin/chef-client') end
    end

  when  '10.11.1'
    sw_id = data_bag_item('software', 'chefdk')['id']
    sw_version = data_bag_item('software', 'chefdk')['version']
    download_url = data_bag_item('software', 'chefdk')['mac1011-download_src']
    dmg_name = data_bag_item('software', 'chefdk')['dmg']

    dmg_package 'chefdk-0.10.0-1' do
      volumes_dir 'Chef Development Kit'
      dmg_name "#{sw_id}-#{sw_version}"
      source "#{download_url}"
      type 'pkg'
      action :install
      not_if do ::File.exist?('/opt/chefdk/bin/chef-client') end
    end
  end
when 'centos'
  case node['platform_version']
  when '6.7'
    sw_id = data_bag_item('software', 'chefdk')['id']
    sw_version = data_bag_item('software', 'chefdk')['version']
    download_url = data_bag_item('software', 'chefdk')['download_src']
    rpm_file = data_bag_item('software', 'chefdk')['centos6_rpm']

    remote_file "/root/#{rpm_file}" do
      source "#{download_url}"
      action :create_if_missing
    end

    bash 'install chefdk' do
      cwd '/root'
      code <<-EOH
      yum -y localinstall /root/#{rpm_file}
      EOH
      not_if do ::File.exist?('/opt/chefdk/bin/chef') end
    end
  end
end
