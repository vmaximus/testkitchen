name             'testkitchen'
maintainer       'DevOps'
maintainer_email 'vphan13@gmail.com'
license          'All rights reserved'
description      'Installs/Configures testkitchen'
long_description 'Installs/Configures test kitchen, vagrant, virtualbox and chefdk on OS X (Yosemite) & CentOS 6.x'
version          '0.1.1'

depends "dmg"
depends "vagrant"
