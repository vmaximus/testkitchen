# testkitchen-cookbook

This cookbook installs all prerequisites 
to get you started with test kitchen

## Supported Platforms

OS X 10.10.5/10.11.1(Yosemite & El Cap) CentOS 6.x

## Attributes
This cookbook uses and includes databags under databags/software.  The versions of chefdk, vagrant and virtualbox are all derived from databag searches at top of each recipe

## Dependencies

This cookbook is dependent on the 'dmg' community cookbook.  This is required to enable installations of Mac .dmg and .pkg files

## Recipes
The recipes below use data bags
* chefdk - installs the chefdk via rpm(linux) or dmg(os x) from Opscode to /opt/chefdk

* vagrant - install vagrant via rpm(linux) or dmg(os x) from Hashicorp to /opt/vagrant, depend on the vagrant community cookbook, used to install vagrant-omnibus plugin

* virtualbox - install virtualbox via rpm(linux) or dmg(os x) from Oracle to /usr/bin

* chef-repo - creates /etc/chef-repo directory with skeleton structure, creates /etc/chef-repo/.chefrc, and adds entries to users .bash_profile

```
chef-repo
├── cookbooks
│   ├── testkitchen
├── data_bags
│   └── software
├── environments
│   └── QA.json
└── roles
```
## Usage
This cookbook makes the following assumptions
The chef-repo has the above directory structure

The Berksfile allows berks to resolve cookbook dependencies, therefore if a cookbook you've written references a recipe in another cookbook, the Berksfile needs to explicitly define a path to the cookbook via a local path.  This is configured in the Berksfile via a ruby loop

The dmg cookbook is a community cookbook and is resolved by the https://chef.supermarket.io string at the top of the file 

The .kitchen.yml file controls what type of hardware/os/cookbooks and recipes are applied to your virtualbox instance

The .kitchen.yml file in this repo references the location of all other locally written cookbooks, data_bags, environments, roles and enables virtio nics(for linux boxes, comment this out for macs).
It is meant as a starting point for your test kitchen instances

The Vagrant file included in this cookbook has pinned a the chef-client to a particular version.

### testkitchen::default

Include `testkitchen` in your node's `run_list`:

```json
{
  "run_list": [
    "recipe[testkitchen::default]"
  ]
}
```

## To Do
Add support for CentOS 7 and Ubuntu flavors
Install vagrant-omnibus plugin to pin chef-client versioin

## License and Authors

Author:: Vinh Phan (vphan13@gmail.com)